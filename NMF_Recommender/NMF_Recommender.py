import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.decomposition import NMF


class NMFRecommender:

    def __init__(self, random_state=15, tol=1e-3, maxiter=200, rank=3):
        """The parameter values for the algorithm"""
        self.random_state = random_state
        self.tol = tol
        self.maxiter = maxiter
        self.rank = rank
       
    def initialize_matrices(self, m, n):
        """Initialize the W and H matrices"""
        np.random.seed(self.random_state)
        W = np.random.random(size=(m,self.rank))
        H = np.random.random(size=(self.rank, n))
        return W, H
        
    def compute_loss(self, V, W, H):
        """Computes the loss of the algorithm according to the frobenius norm"""

        return np.linalg.norm(V-W@H, ord='fro')
    
    def update_matrices(self, V, W, H):
        """The multiplicative update step to update W and H"""
        H_1 = H*((W.T)@V)/((W.T)@W@H)
        W_1 = W*(V@(H_1).T)/(W@H_1@(H_1.T))
        return W_1, H_1

      
    def fit(self, V):
        """Fits W and H weight matrices according to the multiplicative update 
        algorithm. Return W and H"""
        m,n = V.shape
        W, H = self.initialize_matrices(m,n)

        for i in range(self.maxiter):
            W, H = self.update_matrices(V, W, H)
            if self.compute_loss(V, W, H) < self.tol:
                break
        return W, H
        

    def reconstruct(self, W, H):
        """Reconstructs the V matrix for comparison against the original V 
        matrix"""
        return W@H


        
def prob4(rank=2):
    """Run NMF recommender on the grocery store example"""
    V = np.array([[0,1,0,1,2,2],
                  [2,3,1,1,2,2],
                  [1,1,1,0,1,1],
                  [0,2,3,4,1,1],
                  [0,0,0,0,1,0]])
    model = NMFRecommender(rank=rank)
    W, H = model.fit(V)
    Vf = model.reconstruct(W, H)
    return W, H, np.sum(np.argmax(H,axis=0)==1)


def prob5():
    """Calculate the rank and run NMF
    """
    df = pd.read_csv("artist_user.csv")

    # calculate best rank
    benchmark = 0.0001*np.linalg.norm(df, ord='fro')
    val = benchmark + 1
    rank = 2
    while val >= benchmark:
        rank += 1 # increment every time
        model = NMF(n_components=rank, init='random', random_state=0)
        W = model.fit_transform(df)
        H = model.components_
        V = W@H
        val = np.sqrt(mean_squared_error(df, V))
    
    return rank, V


def discover_weekly(user_id, V):
    """
    Create the recommended weekly 30 list for a given user
    """
    # download user and artist data
    df = pd.read_csv("artist_user.csv")
    artists = pd.read_csv("artists.csv")

    row_num = df[df['Unnamed: 0'] == user_id].index[0] # get row person is in
    # get indices of artists the person has listened to
    listened = np.array(df)[row_num, 1:] > 0
    # set the recommendation for already listened peeps to zeros
    V[row_num, 1:][listened] = 0 

    # get indices of top recommended (but never-heard) artists
    ind = np.argsort(V[row_num, 1:])[::-1][:30]
    
    return np.array(artists.iloc[ind]['name'])
    
