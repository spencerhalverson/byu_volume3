"""
Random Forest Lab

Spencer Halverson
MATH 403
9/28/2021
"""
import graphviz
import os
from uuid import uuid4

from sklearn.ensemble import RandomForestClassifier
import time
import numpy as np



# Problem 1
class Question:
    """Questions to use in construction and display of Decision Trees.
    Attributes:
        column (int): which column of the data this question asks
        value (int/float): value the question asks about
        features (str): name of the feature asked about
    Methods:
        match: returns boolean of if a given sample answered T/F"""
    
    def __init__(self, column, value, feature_names):
        self.column = column
        self.value = value
        self.features = feature_names[self.column]
    
    def match(self,sample):
        """Returns T/F depending on how the sample answers the question
        Parameters:
            sample ((n,), ndarray): New sample to classify
        Returns:
            (bool): How the sample compares to the question"""
        return (sample[self.column] >= self.value) 
        
    def __repr__(self):
        return "Is %s >= %s?" % (self.features, str(self.value))
    
def partition(data,question):
    """Splits the data into left (true) and right (false)
    Parameters:
        data ((m,n), ndarray): data to partition
        question (Question): question to split on
    Returns:
        left ((j,n), ndarray): Portion of the data matching the question
        right ((m-j, n), ndarray): Portion of the data NOT matching the question
    """
    left = []
    right = []
    for i in data:
        if question.match(i):
            left.append(i)
        else:
            right.append(i)

    # return None if list is empty
    if len(left) == 0:
        left = None
    else:
        left = np.array(left)
    if len(right)== 0 :
        right = None
    else:
        right = np.array(right)
    return left, right
    
#Problem 2    
def gini(data):
    """Return the Gini impurity of given array of data.
    Parameters:
        data (ndarray): data to examine
    Returns:
        (float): Gini impurity of the data"""
    labels = data[:, -1]
    samples = data[:, :-1]

    # find ratios of each label
    values, counts = np.unique(labels, return_counts=True)
    total = np.sum(counts)
    ratios = counts/total

    # compute Gini impurity
    impurity = 1.0
    for i in ratios:
        impurity -= i**2

    return impurity

def info_gain(left,right,G):
    """Return the info gain of a partition of data.
    Parameters:
        left (ndarray): left split of data
        right (ndarray): right split of data
        G (float): Gini impurity of unsplit data
    Returns:
        (float): info gain of the data"""
    # compute impurity and lengths for each split
    left_impurity = gini(left)
    right_impurity = gini(right)
    left_len = len(left)
    right_len = len(right)
    total_len = left_len + right_len

    return G - left_len/total_len*left_impurity - right_len/total_len*right_impurity
    

# Problem 3, Problem 7
def find_best_split(data, feature_names, min_samples_leaf=5, random_subset=False):
    """Find the optimal split
    Parameters:
        data (ndarray): Data in question
        feature_names (list of strings): Labels for each column of data
        min_samples_leaf (int): minimum number of samples per leaf
        random_subset (bool): for Problem 7
    Returns:
        (float): Best info gain
        (Question): Best question"""
    # track our best info gain
    best_info = 0
    best_question = None

    # choose subset of features if we have random subset
    if random_subset:
        feature_indices = np.random.choice(range(len(feature_names)-1), int(np.sqrt(len(feature_names)-1)), replace=False)
    else:
        feature_indices = range(len(feature_names) - 1)


    # go through all unique values for all features
    for feature_index in feature_indices:
        for unique_val in np.unique(data[:, feature_index]):
            # create question
            q = Question(feature_index, unique_val, feature_names)
            # partition on question
            l, r = partition(data, q)

            # check for valid leaves
            if l is None or r is None or len(r) < min_samples_leaf or len(l) < min_samples_leaf:
                continue

            # store new best info gain / question
            G = gini(data)
            info = info_gain(l, r, gini(data))
            if info > best_info:
                best_info = info
                best_question = q
    

    if best_question == None:
        return 0, 0

    return best_info, best_question

# Problem 4
class Leaf:
    """Tree leaf node
    Attribute:
        prediction (dict): Dictionary of labels at the leaf"""
    def __init__(self,data):
        # count each label and add to `prediction`
        vals, counts = np.unique(data[:, -1], return_counts=True)
        self.prediction = dict(zip(vals, counts))
            
class Decision_Node:
    """Tree node with a question
    Attributes:
        question (Question): Question associated with node
        left (Decision_Node or Leaf): child branch
        right (Decision_Node or Leaf): child branch"""
    def __init__(self, question, left_branch, right_branch):
        self.question = question
        self.left = left_branch
        self.right = right_branch

## Code to draw a tree
def draw_node(graph, my_tree):
    """Helper function for drawTree"""
    node_id = uuid4().hex
    #If it's a leaf, draw an oval and label with the prediction
    if isinstance(my_tree, Leaf):
        graph.node(node_id, shape="oval", label="%s" % my_tree.prediction)
        return node_id
    else: #If it's not a leaf, make a question box
        graph.node(node_id, shape="box", label="%s" % my_tree.question)
        left_id = draw_node(graph, my_tree.left)
        graph.edge(node_id, left_id, label="T")
        right_id = draw_node(graph, my_tree.right)    
        graph.edge(node_id, right_id, label="F")
        return node_id

def draw_tree(my_tree):
    """Draws a tree"""
    #Remove the files if they already exist
    for file in ['Digraph.gv','Digraph.gv.pdf']:
        if os.path.exists(file):
            os.remove(file)
    graph = graphviz.Digraph(comment="Decision Tree")
    draw_node(graph, my_tree)
    graph.render(view=True) #This saves Digraph.gv and Digraph.gv.pdf

# Problem 5
def build_tree(data, feature_names, min_samples_leaf=5, max_depth=4, current_depth=0, random_subset=False):
    """Build a classification tree using the classes Decision_Node and Leaf
    Parameters:
        data (ndarray)
        feature_names(list or array)
        min_samples_leaf (int): minimum allowed number of samples per leaf
        max_depth (int): maximum allowed depth
        current_depth (int): depth counter
        random_subset (bool): whether or not to train on a random subset of features
    Returns:
        Decision_Node (or Leaf)"""
    # if we are too deep
    if current_depth == max_depth or len(data)<2*min_samples_leaf:
        return Leaf(data)
    info_gain, question = find_best_split(data, feature_names, min_samples_leaf, random_subset)    
    if info_gain == 0:
        return Leaf(data)
    else:
        left, right = partition(data, question)
        left_branch = build_tree(left, feature_names, min_samples_leaf, max_depth, current_depth+1)
        right_branch = build_tree(right, feature_names, min_samples_leaf, max_depth, current_depth+1)
        return Decision_Node(question, left_branch, right_branch)
# Problem 6
def predict_tree(sample, my_tree):
    """Predict the label for a sample given a pre-made decision tree
    Parameters:
        sample (ndarray): a single sample
        my_tree (Decision_Node or Leaf): a decision tree
    Returns:
        Label to be assigned to new sample"""
    # Ask question if this node is Decision
    if isinstance(my_tree, Decision_Node):
        # print(sample, my_tree.question)
        if my_tree.question.match(sample):
            return predict_tree(sample, my_tree.left)
        else:
            return predict_tree(sample, my_tree.right)
    elif isinstance(my_tree, Leaf):
        max_key = max(my_tree.prediction, key=my_tree.prediction.get)
        return max_key
    else:
        print("AAAAAAAHGHGAHGAHGA")
    
def analyze_tree(dataset,my_tree):
    """Test how accurately a tree classifies a dataset
    Parameters:
        dataset (ndarray): Labeled data with the labels in the last column
        tree (Decision_Node or Leaf): a decision tree
    Returns:
        (float): Proportion of dataset classified correctly"""
    total = len(dataset)
    correct = 0
    for row in dataset:
        prediction = predict_tree(row, my_tree)
        answer = row[-1]
        if int(prediction) == int(answer):
            correct += 1
    return correct/total

# Problem 7
def predict_forest(sample, forest):
    """Predict the label for a new sample, given a random forest
    Parameters:
        sample (ndarray): a single sample
        forest (list): a list of decision trees
    Returns:
        Label to be assigned to new sample"""

    # let each tree vote
    votes = [predict_tree(sample, tree) for tree in forest]
    # find plurality vote
    vals, counts = np.unique(votes, return_counts=True)
    return vals[np.argmax(counts)]

def analyze_forest(dataset,forest):
    """Test how accurately a forest classifies a dataset
    Parameters:
        dataset (ndarray): Labeled data with the labels in the last column
        forest (list): list of decision trees
    Returns:
        (float): Proportion of dataset classified correctly"""
    total = len(dataset)
    correct = 0
    for row in dataset:
        prediction = predict_forest(row, forest)
        answer = row[-1]
        if int(prediction) == int(answer):
            correct += 1
    return correct/total

# Problem 8
def prob8():
    """Use the file parkinsons.csv to analyze a 5 tree forest.
    
    Create a forest with 5 trees and train on 100 random samples from the dataset.
    Use 30 random samples to test using analyze_forest() and SkLearn's 
    RandomForestClassifier.
    
    Create a 5 tree forest using 80% of the dataset and analzye using 
    RandomForestClassifier.
    
    Return three tuples, one for each test.
    
    Each tuple should include the accuracy and time to run: (accuracy, running time) 
    """
    # load data
    sample_data = np.loadtxt('parkinsons.csv', delimiter=',')[:, 1:]
    park_features = np.loadtxt('parkinsons_features.csv', 
                               delimiter=',', dtype=str)[1:]
    # choose sample, split train and test sets
    np.random.shuffle(sample_data)
    train = sample_data[:100]
    test = sample_data[100:130]

    num_trees = 5
    min_samples = 15

    # generate forest
    start = time.time()
    forest = []
    
    for i in range(num_trees):
        forest.append(build_tree(train, park_features, min_samples_leaf=min_samples, random_subset=True))

    my_acc = analyze_forest(test, forest)
    my_time = time.time()-start

    # sk does it on small sample
    start = time.time()
    sk_forest = RandomForestClassifier(n_estimators=num_trees, max_depth=4, min_samples_leaf=min_samples)
    sk_forest.fit(train[:, :-1], train[:, -1])
    sk_acc = sk_forest.score(test[:, :-1], test[:, -1])
    sk_time = time.time() - start

    ## sk on full sample
    start = time.time()
    n = len(sample_data)
    split = int(0.8*n)
    np.random.shuffle(sample_data)

    # split test/train
    train = sample_data[:split]
    test = sample_data[split:]
    
    # train and find accuracy
    sk_full_forest = RandomForestClassifier()
    sk_full_forest.fit(train[:, :-1], train[:, -1])
    sk_full_acc = sk_forest.score(test[:, :-1], test[:, -1])
    sk_full_time = time.time() - start

    return (my_acc, my_time), (sk_acc, sk_time), (sk_full_acc, sk_full_time)

    
    

