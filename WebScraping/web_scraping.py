"""Volume 3: Web Scraping.
Spencer Halverson
MATH 405
2022-01-18
"""

import requests
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Problem 1
def prob1():
    """Use the requests library to get the HTML source for the website 
    http://www.example.com.
    Save the source as a file called example.html.
    If the file already exists, do not scrape the website or overwrite the file.
    """
    try:
        with open('example.html', mode='x') as myfile: # this will do nothing if the file exists
            response = requests.get("http://www.example.com")
            if response.status_code == 200:
                myfile.write(response.text)

        return
    except: # if file exists, do nothing
        return
 
    
# Problem 2
def prob2(code):
    """Return a list of the names of the tags in the given HTML code.
    Parameters:
        code (str): A string of html code
    Returns:
        (list): Names of all tags in the given code"""

    soup = BeautifulSoup(code, "html.parser")
    tags = soup.find_all(True)
    names = [t.name for t in tags]
    return names



# Problem 3
def prob3(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Return the
    text of the first <a> tag and whether or not it has an href
    attribute.
    Parameters:
        filename (str): Filename to open
    Returns:
        (str): text of first <a> tag
        (bool): whether or not the tag has an 'href' attribute
    """
    # load an HTML file
    with open(filename, 'r') as myfile:
        text = myfile.read()
        
        # put it in the soup object
        soup = BeautifulSoup(text, "html.parser")
        a_tag = soup.a
        has_href = False
        if "href" in a_tag.attrs:
            has_href = True
        return a_tag.string, has_href

    raise NotImplementedError("Problem 3 Incomplete")


# Problem 4
def prob4(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    with open(filename, 'r') as myfile:
        text = myfile.read()
        # put it in the soup object
        soup = BeautifulSoup(text, "html.parser")

        date = soup.find(string = "Thursday, January 1, 2015").parent
        link_p = soup.find(class_="previous-link").contents[0]
        link_n = soup.find(class_="next-link").contents[0]
        temp = soup.find(string="59").parent

        return date, link_p, link_n, temp

        


# Problem 5
def prob5(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    with open(filename, 'r') as myfile:
        text = myfile.read()
        # put it in the soup object
        soup = BeautifulSoup(text, "html.parser")

        weblinks = soup.select("a[href^=http://www.federalreserve.gov/releases/lbr/20]")
        final_weblinks = []

        # sort out the ones that didn't have dates
        for i in weblinks:
            if "20" in i.string:
            
                final_weblinks.append(i)
        return final_weblinks


# Problem 6
def prob6(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """
    # read file into soup object
    with open(filename, 'r') as myfile:
        text = myfile.read()
        soup = BeautifulSoup(text, "html.parser")

        # find each bank row
        names = []
        domestics = []
        foreigns = []
        rows = soup.p.table.tbody.find_all("tr")
        for i, row in enumerate(rows):
            tds = row.find_all("td")
            # print(tds)
            try:
                
                domestics.append(int(tds[9].string.replace(",", "")))
                foreigns.append(int(tds[10].string.replace(",", "")))
                names.append(tds[0].string)
            except:
                pass
    # get DataFrame with only relevant data
    data = np.array([names,domestics,foreigns]).T
    df= pd.DataFrame(data = data, columns = ["Name", "Domestic", "Foreign"])
    df["Domestic"] = df.Domestic.astype("int")
    df["Foreign"] = df.Foreign.astype("int")

    # plot
    fig, axs = plt.subplots(2)

    # sort by top domestics
    df.drop(columns="Foreign").sort_values(by="Domestic", ascending=False)[:7].plot(
                                                            ax=axs[0],
                                                             kind="barh", 
        title="Top 7 Banks by # of Domestic Branches", x = "Name", y="Domestic")
    # sort by top foreigns
    df.drop(columns="Domestic").sort_values(by="Foreign", ascending=False)[:7].plot(
                                                            ax=axs[1],
                                                             kind="barh", 
        title="Top 7 Banks by # of Foreign Branches", x = "Name", y="Foreign")
    plt.tight_layout()
    plt.show()
