# Spencer Halverson
# 2022-03-15
# MATH 405

from scipy.stats.distributions import norm
from scipy.stats import multivariate_normal
from scipy.optimize import fmin, minimize
import numpy as np
import matplotlib.pyplot as plt

import statsmodels
from statsmodels.tsa.api import VARMAX
import statsmodels.api as sm
import statsmodels.datasets as datasets
from statsmodels.tsa.arima.model import ARIMA
import pandas as pd
from statsmodels.tsa.base.datetools import dates_from_str

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

def arma_forecast_naive(file='weather.npy',p=2,q=1,n=20):
    """
    Perform ARMA(1,1) on data. Let error terms be drawn from
    a standard normal and let all constants be 1.
    Predict n values and plot original data with predictions.

    Parameters:
        file (str): data file
        p (int): order of autoregressive model
        q (int): order of moving average model
        n (int): number of future predictions
    """
    # given constants
    phi = 0.5
    theta = 0.1

    # load data
    data = np.load(file)
    diff = np.diff(data, 1)
    base = len(diff)
    diff_list = diff.tolist()

    # find eps
    eps = np.random.normal(size=(len(diff))).tolist()
    
    # build arma(p,q) model
    for t in range(base, base+n):
        c = 0
        AR = 0
        for i in range(1, p+1):
            AR += phi*diff_list[t-i]
        MA = 0
        for j in range(1, q+1):
            MA += theta*eps[t-j]
        new_eps = np.random.normal()
        diff_list.append(c + AR + MA + new_eps)
        eps.append(new_eps)
    
    # plot
    plt.plot(np.arange(base), diff_list[:base], color='blue', label="Old Data")
    plt.plot(np.arange(base, base+n), diff_list[base:], color='orange', label="New Data")
    plt.legend()
    plt.xlabel("Day of the Month")
    plt.ylabel("Change in Temperature (C) - mu = 0")
    plt.title("Problem 1")
    plt.show()

def arma_likelihood(file='weather.npy', phis=np.array([0.9]), 
                    thetas=np.array([0]), mu=17., std=0.4):
    """
    Transfer the ARMA model into state space. 
    Return the log-likelihood of the ARMA model.

    Parameters:
        file (str): data file
        phis (ndarray): coefficients of autoregressive model
        thetas (ndarray): coefficients of moving average model
        mu (float): mean of errorm
        std (float): standard deviation of error

    Return:
        log_likelihood (float)
    """

    # load data
    data = np.load(file)
    diff = np.diff(data, 1)
    base = len(diff)

    # get F, Q, H
    F, Q, H, dim_states, dim_time_series = state_space_rep(phis, thetas, mu, std)

    # get means and covariance matrices from Kalman filter
    mus, covs = kalman(F, Q, H, diff - mu)

    # get log probability 
    log_prob = 0
    for t in range(base):
        try:
#             log_prob += norm.logpdf(x=diff[t], mean=H@mus[t] + mu, cov=H@covs[t]@(H.T))
            log_prob += multivariate_normal.logpdf(x=diff[t], mean=H@mus[t] + mu, cov=H@covs[t]@(H.T))
        except:
            pass
    return float(log_prob)


def model_identification(file='weather.npy',p=4,q=4):
    """
    Identify parameters to minimize AIC of ARMA(p,q) model

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model

    Returns:
        phis (ndarray (p,)): coefficients for AR(p)
        thetas (ndarray (q,)): coefficients for MA(q)
        mu (float): mean of error
        std (float): std of error
    """
    # load data
    data = np.load(file)
    diff = np.diff(data, 1)
    base = len(diff)

    min_AIC = np.inf
    best_p = 1
    best_q = 1
    best_model = None

    for i in range(1, p+1):
        for j in range(1, q+1):
            
            # assume p, q, and time_series are defined
            def f(x): # x contains the phis, thetas, mu, and std
                return -1*arma_likelihood(file, phis=x[:i], thetas=x[i:i+j], mu=x[-2],std=x[-1])
            
            # create initial point
            x0 = np.random.normal(0,0.0001,i+j+2)
            # x0 = np.zeros(i+j+2)
            x0[-2] = diff.mean()
            x0[-1] = diff.std()

            sol = minimize(f,x0, method = "SLSQP")
            sol = sol['x']
            
            # compute AIC
            k = i+j+2
            n = base
            likelihood = arma_likelihood(file, np.array(sol[:i]), np.array(sol[i:i+j]), sol[-2], sol[-1])
            aic = 2*k*(1+ (k+1)/(n-k)) - 2*likelihood
            if aic <= min_AIC:
                min_AIC = aic
                best_sol = sol
                
#             try:
#                 # compute AIC for arima model
#                 model = ARIMA(diff.copy(),order=(i, 0, j), trend='c').fit(method='innovations_mle') # set d=0 to get ARMA
#                 aic = model.aic
#                 if aic <= min_AIC:
#                     min_AIC = aic
#                     best_sol = sol
#                     best_model = model
#                     best_p = model.specification.k_ar
#                     best_q = model.specification.k_ma
                    
# #                     print(aic)
# #                     print(np.array(best_sol[:i]), np.array(best_sol[i:i+j]), best_sol[-2], best_sol[-1])
#             except:
#                 continue
                
    return np.array(best_sol[:i]), np.array(best_sol[i:i+j]), best_sol[-2], best_sol[-1]



def arma_forecast(file='weather.npy', phis=np.array([0]), thetas=np.array([0]), mu=0., std=0., n=30):
    """
    Forecast future observations of data.
    
    Parameters:
        file (str): data file
        phis (ndarray (p,)): coefficients of AR(p)
        thetas (ndarray (q,)): coefficients of MA(q)
        mu (float): mean of ARMA model
        std (float): standard deviation of ARMA model
        n (int): number of forecast observations

    Returns:
        new_mus (ndarray (n,)): future means
        new_covs (ndarray (n,)): future standard deviations
    """
    # load data
    data = np.load(file)
    diff = np.diff(data, 1)
    base = len(diff)
    diff_list = diff.tolist()

    # get F, Q, H
    F, Q, H, dim_states, dim_time_series = state_space_rep(phis, thetas, mu, std)

    # get means and covariance matrices from Kalman filter
    mus, covs = kalman(F, Q, H, diff - mu)
    mus = mus.tolist()
    covs = covs.tolist()

    # calculate x_n and P_n
    y_n = diff[-1] - H@mus[-1]
    S_n = H@covs[-1]@(H.T) 
    K = covs[-1]@(H.T)@(np.linalg.inv(S_n))
    mus.append(mus[-1] + K@y_n)
    covs.append((np.eye((K@H).shape[0])-K@H)@covs[-1])



    # build arma(p,q) model
    for t in range(base, base+n):
        mus.append(F@mus[-1])

    mus=np.array(mus)
    stds = mus[base+1:,0].std()

    
    # plot
    plt.plot(np.arange(base), diff_list[:base], color='blue', label="Old Data")
    plt.plot(np.arange(base, base+n), mus[base+1:, 0], color='orange', linestyle="--", label="Forecase")
    plt.plot(np.arange(base, base+n), mus[base+1:, 0]+2*stds, color='green', label="95% Confidence Interval")
    plt.plot(np.arange(base, base+n), mus[base+1:, 0]-2*stds, color='green')
    
    plt.legend()
    plt.title("Problem 4")
    plt.show()

def sm_arma(file = 'weather.npy', p=3, q=3, n=30):
    """
    Build an ARMA model with statsmodel and 
    predict future n values.

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model
        n (int): number of values to predict

    Return:
        aic (float): aic of optimal model
    """
    # load data
    data = np.load(file)
    diff = np.diff(data, 1)
    base = len(diff)

    min_AIC = np.inf
    best_p = 1
    best_q = 1
    best_model = None

    for i in range(1, p+1):
        for j in range(1, q+1):
            
            # assume p, q, and time_series are defined
            def f(x): # x contains the phis, thetas, mu, and std
                return -1*arma_likelihood(file, phis=x[:i], thetas=x[i:i+j], mu=x[-2],std=x[-1])
            
            # create initial point
            x0 = np.random.normal(0,0.0001,i+j+2)
            # x0 = np.zeros(i+j+2)
            x0[-2] = diff.mean()
            x0[-1] = diff.std()

            sol = minimize(f,x0, method = "SLSQP")
            sol = sol['x']
            
                
            try:
                # compute AIC for arima model
                model = ARIMA(diff.copy(),order=(i, 0, j), trend='c').fit(method='innovations_mle') # set d=0 to get ARMA
                aic = model.aic
                if aic <= min_AIC:
                    min_AIC = aic
                    best_sol = sol
                    best_model = model
                    best_p = model.specification.k_ar
                    best_q = model.specification.k_ma
                    
#                     print(aic)
#                     print(np.array(best_sol[:i]), np.array(best_sol[i:i+j]), best_sol[-2], best_sol[-1])
            except:
                continue
            
    # plot n future observations of best model
    prediction = best_model.predict(0, len(diff)+n)
    plt.plot(np.arange(len(diff)), diff, label="Old Data")
    plt.plot(np.arange(len(prediction)), prediction, label="ARMA Model")
    
    plt.title("Statsmodel ARMA({},{})".format(best_p, best_q))
    plt.xlabel("Day of the Month")
    plt.ylabel("Change in Temperature (C) - mu = 0")
    plt.legend(loc='best')
    plt.show()    
    
    return min_AIC

def sm_varma(start='1959-03-31', end='2012-09-30'):
    """Use the statsmodels VARMAX class to forecast on macroeconomic data 
    between the start and end dates. Use AIC as the criterion for model 
    selection when fitting the model. Plot the prediction, original data 
    and a 95% confidence interval (2 standard deviations away from the mean)
    around the future observations. Return the AIC of the chosen model. """
    
    # Load in data
    df = datasets.macrodata.load_pandas().data
    # Create DatetimeIndex
    dates = df[['year', 'quarter']].astype(int).astype(str)
    dates = dates["year"] + "Q" + dates["quarter"]
    dates = dates_from_str(dates)
    df.index = pd.DatetimeIndex(dates, freq='infer')
    
    # Select columns used in prediction
    df = df[['realgdp','realcons','realinv']]
    
    # Initialize and fit model
    mod = VARMAX(df)
    mod = mod.fit(maxiter=1000, disp=False, ic = 'aic')
    aic = mod.aic
    
    # Predict
    pred = mod.predict(start, end)
    # Get confidence intervals
    forecast_obj = mod.get_forecast(end)
    all_CI = forecast_obj.conf_int(alpha=0.05)
    
    
    # Plot predictions against true values
    plt.figure(figsize=(10,10))
    
    # Plot GDP
    plt.subplot(311)
    plt.plot(df['realgdp'], label = 'realgdp')
    plt.plot(pred['realgdp'], label="forecast")
    plt.plot(all_CI['lower realgdp'], 'k--', label="95% confidence interval")
    plt.plot(all_CI['upper realgdp'], 'k--')
    plt.legend()
    plt.title('realgdp prediction')
    
    # Plot consumption
    plt.subplot(312)
    plt.plot(df['realcons'], label = 'realcons')
    plt.plot(pred['realcons'], label="forecast")
    plt.plot(all_CI['lower realcons'], 'k--', label="95% confidence interval")
    plt.plot(all_CI['upper realcons'], 'k--')
    plt.legend()
    plt.title('realcons prediction')
    
    # Plotdomestic investment
    plt.subplot(313)
    plt.plot(df['realinv'], label = 'realinv')
    plt.plot(pred['realinv'], label="forecast")
    plt.plot(all_CI['lower realinv'], 'k--', label="95% confidence interval")
    plt.plot(all_CI['upper realinv'], 'k--')
    plt.legend()
    plt.title('realinv prediction')
    
    plt.show()
    return aic  


def manaus(start='1983-01-31',end='1995-01-31',p=4,q=4):
    """
    Plot the ARMA(p,q) model of the River Negro height
    data using statsmodels built-in ARMA class.

    Parameters:
        start (str): the data at which to begin forecasting
        end (str): the date at which to stop forecasting
        p (int): max_ar parameter
        q (int): max_ma parameter
    Return:
        aic_min_order (tuple): optimal order based on AIC
        bic_min_order (tuple): optimal order based on BIC
    """
    # Get dataset
    raw = pydata('manaus')
    # Make DateTimeIndex
    manaus = pd.DataFrame(raw.values,index=pd.date_range('1903-01','1993-01',freq='M'))
    manaus = manaus.drop(0,axis=1)
    # Reset column names
    manaus.columns = ['Water Level']

    raise NotImplementedError("Problem 6 Incomplete")

###############################################################################
    
def kalman(F, Q, H, time_series):
    # Get dimensions
    dim_states = F.shape[0]

    # Initialize variables
    # covs[i] = P_{i | i-1}
    covs = np.zeros((len(time_series), dim_states, dim_states))
    mus = np.zeros((len(time_series), dim_states))

    # Solve of for first mu and cov
    covs[0] = np.linalg.solve(np.eye(dim_states**2) - np.kron(F,F),np.eye(dim_states**2)).dot(Q.flatten()).reshape(
            (dim_states,dim_states))
    mus[0] = np.zeros((dim_states,))

    # Update Kalman Filter
    for i in range(1, len(time_series)):
        t1 = np.linalg.solve(H.dot(covs[i-1]).dot(H.T),np.eye(H.shape[0]))
        t2 = covs[i-1].dot(H.T.dot(t1.dot(H.dot(covs[i-1]))))
        covs[i] = F.dot((covs[i-1] - t2).dot(F.T)) + Q
        mus[i] = F.dot(mus[i-1]) + F.dot(covs[i-1].dot(H.T.dot(t1))).dot(
                time_series[i-1] - H.dot(mus[i-1]))
    return mus, covs

def state_space_rep(phis, thetas, mu, sigma):
    # Initialize variables
    dim_states = max(len(phis), len(thetas)+1)
    dim_time_series = 1 #hardcoded for 1d time_series

    F = np.zeros((dim_states,dim_states))
    Q = np.zeros((dim_states, dim_states))
    H = np.zeros((dim_time_series, dim_states))

    # Create F
    F[0][:len(phis)] = phis
    F[1:,:-1] = np.eye(dim_states - 1)
    # Create Q
    Q[0][0] = sigma**2
    # Create H
    H[0][0] = 1.
    H[0][1:len(thetas)+1] = thetas

    return F, Q, H, dim_states, dim_time_series