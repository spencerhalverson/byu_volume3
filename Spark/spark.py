# solutions.py

import pyspark
from pyspark.sql import SparkSession
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler, StringIndexer, OneHotEncoder
from pyspark.ml.tuning import ParamGridBuilder, TrainValidationSplit
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator as MCE
import random



# --------------------- Resilient Distributed Datasets --------------------- #

### Problem 1
def word_count(filename='huck_finn.txt'):
    """
    A function that counts the number of occurrences unique occurrences of each
    word. Sorts the words by count in descending order.
    Parameters:
        filename (str): filename or path to a text file
    Returns:
        word_counts (list): list of (word, count) pairs for the 20 most used words
    """ 
    # Start Spark Session
    spark = SparkSession.builder.appName("app_name").getOrCreate()

    # Load text file
    word_file = spark.sparkContext.textFile(filename)
    

    # Sort into (word, count) pairs by count
    text = word_file.flatMap(lambda row: row.split(' '))
    pairRDD = text.map(lambda word: (word, 1))
    words = pairRDD.reduceByKey(lambda x, y: x+y)
    word_count = words.sortBy(lambda row: row[1]).collect()
    
    # stop Session
    spark.stop()

    # return top 20 words
    return word_count[::-1][:20]
    
    
### Problem 2
def monte_carlo(n=10**5, parts=6):
    """
    Runs a Monte Carlo simulation to estimate the value of pi.
    Parameters:
        n (int): number of sample points per partition
        parts (int): number of partitions
    Returns:
        pi_est (float): estimated value of pi
    """
    # Start Spark Session
    spark = SparkSession.builder.appName("app_name").getOrCreate()

    # run Monte Carlo to estimate pi
    points = np.random.random((n*parts, 2))*2 - 1
    points_parallelized = spark.sparkContext.parallelize(points, parts)
    inside_circle = points_parallelized.map(lambda val: 1*(np.linalg.norm(val) <= 1))
    percent = inside_circle.reduce(lambda x, y: x+y)/(n*parts)*4

    # stop session
    spark.stop()

    return percent



# ------------------------------- DataFrames ------------------------------- #

### Problem 3
def titanic_df(filename='titanic.csv'):
    """
    Calculates some statistics from the titanic data.
    
    Returns: the number of women on-board, the number of men on-board,
             the survival rate of women, 
             and the survival rate of men in that order.
    """
    # Start Spark Session
    spark = SparkSession.builder.appName("app_name").getOrCreate()

    # load titanic data into Spark DataFrame
    schema = ('survived INT, pclass INT, name STRING, sex STRING, age FLOAT, sibsp INT, parch INT, fare FLOAT')
    titanic = spark.read.csv(filename, schema=schema)

    # find number of men and women total, and survival by sex
    total_men_women = titanic.groupBy('sex').count()
    survival_by_sex = titanic.groupBy('sex', 'survived').count()\
        .sort('sex', 'survived', ascending=False).filter(titanic.survived.between(0.5, 1.5))
    
    answer_dict = {}
    for row in survival_by_sex.collect():
        if row[0] == 'male':
            answer_dict['male_survive'] = row[2]
        elif row[0] == 'female':
            answer_dict['female_survive'] = row[2]

    for row in total_men_women.collect():
        if row[0] == 'male':
            answer_dict['male_count'] = row[1]
        elif row[0] == 'female':
            answer_dict['female_count'] = row[1]
    
 
    answers = [answer_dict['female_count'], answer_dict['male_count'], answer_dict['female_survive']/answer_dict['female_count'], answer_dict['male_survive']/answer_dict['male_count']]
    
    # stop Spark Session
    spark.stop()
    
    return answers[0], answers[1], answers[2], answers[3]

   


### Problem 4
def crime_and_income(crimefile='london_crime_by_lsoa.csv',
                     incomefile='london_income_by_borough.csv', 
                     major_cat='Murder'):
    """
    Explores crime by borough and income for the specified min_cat
    Parameters:
        crimefile (str): path to csv file containing crime dataset
        incomefile (str): path to csv file containing income dataset
        major_cat (str): crime minor category to analyze
    returns:
        numpy array: borough names sorted by percent months with crime, descending
    """
    # Start Spark Session
    spark = SparkSession.builder.appName("app_name").getOrCreate()

    crime = spark.read.csv(crimefile, header=True, inferSchema=True)
    income = spark.read.csv(incomefile, header=True, inferSchema=True)

    # group crimes by borough
    crime_group = crime.groupBy('borough')\
        .sum('value')\
        .sort("sum(value)", ascending=False)\
        .withColumnRenamed("sum(value)", "total_crime")
    income_group = income.select('borough', 'median-08-16')
    combined = crime_group.join(income_group, on = 'borough')
    combined.show()

    data = np.array(combined.collect())
    crime_val = [float(i) for i in data[:, 1]]
    income_val = [float(i) for i in data[:, 2]]
    plt.scatter(income_val, crime_val)
    plt.title("Occurrence of {} by borough median income".format(major_cat))
    plt.ylabel("Occurrence of {}".format(major_cat))
    plt.xlabel("Median income of borough")
    
    plt.show()


    # stop session
    spark.stop()
    return data


### Problem 5
def titanic_classifier(filename='titanic.csv'):
    """
    Implements a classifier model to predict who survived the Titanic.
    Parameters:
        filename (str): path to the dataset
    Returns:
        metrics (list): a list of metrics gauging the performance of the model
            ('accuracy', 'weightedPrecision', 'weightedRecall')
    """
    # Start Spark Session
    spark = SparkSession.builder.appName("app_name").getOrCreate()

    # load titanic data into Spark DataFrame
    schema = ('survived INT, pclass INT, name STRING, sex STRING, age FLOAT, sibsp INT, parch INT, fare FLOAT')
    titanic = spark.read.csv(filename, schema=schema)

    # prepare data
    # convert the 'sex' column to binary categorical variable
    sex_binary = StringIndexer(inputCol='sex', outputCol='sex_binary')
    
    # one-hot-encode pclass (Spark automatically drops a column)
    onehot = OneHotEncoder(inputCols=['pclass'], outputCols=['pclass_onehot'])
    
    # create single features column
    features = ['sex_binary', 'pclass_onehot', 'age', 'sibsp', 'parch', 'fare']
    features_col = VectorAssembler(inputCols=features, outputCol='features')

    # now we create a transformation pipeline to apply the operations above
    # this is very similar to the pipeline ecosystem in sklearn
    pipeline = Pipeline(stages=[sex_binary, onehot, features_col])
    titanic = pipeline.fit(titanic).transform(titanic)

    # drop unnecessary columns for cleaner display (note the new columns)
    titanic = titanic.drop('pclass', 'name', 'sex')

    # split into train/test sets (75/25)
    train, test = titanic.randomSplit([0.75, 0.25], seed=11)

    # initialize random forest
    rf = RandomForestClassifier(labelCol='survived', featuresCol='features')

    # run a train-validation-split to fit best elastic net param
    # ParamGridBuilder constructs a grid of parameters to search over.
    paramGrid = ParamGridBuilder()\
        .addGrid(rf.maxDepth, [2, 4, 6])\
        .addGrid(rf.numTrees, [20, 50, 100])\
        .addGrid(rf.minInstancesPerNode, [1, 3, 5])\
        .build()
    # TrainValidationSplit will try all combinations and determine best model using
    # the evaluator (see also CrossValidator)
    tvs = TrainValidationSplit(estimator=rf,
                               estimatorParamMaps=paramGrid,
                               evaluator=MCE(labelCol='survived'),
                               trainRatio=0.75,
                               seed=11)
    # we train the classifier by fitting our tvs object to the training data
    clf = tvs.fit(train)

    # use the best fit model to evaluate the test data
    results = clf.bestModel.evaluate(test)
    # results.predictions.select(['survived', 'prediction']).show(5)

    # performance information is stored in various attributes of "results"
    answers = []
    answers.append(results.accuracy)
    answers.append(results.weightedRecall)
    answers.append(results.weightedPrecision)

    # stop spark session
    spark.stop()

    return answers

