from ipyparallel import Client
import time
import matplotlib.pyplot as plt
import numpy as np

# iPyParallel - Intro to Parallel Programming

# Problem 1
def initialize():
    """
    Write a function that initializes a Client object, creates a Direct
    View with all available engines, and imports scipy.sparse as spar on
    all engines. Return the DirectView.
    """
    # initialize client with Direct View
    client = Client()
    dview = client[:]
    dview.execute("import scipy.sparse as sparse")
    return dview

# Problem 2
def variables(dx):
    """
    Write a function variables(dx) that accepts a dictionary of variables. Create
    a Client object and a DirectView and distribute the variables. Pull the variables back and
    make sure they haven't changed. Remember to include blocking.
    """
    dview = initialize()
    dview.block = True
    dview.push(dx)
    for i in dx.keys():
        dview.pull(i)

# Problem 3
def prob3(n=1000000):
    """
    Write a function that accepts an integer n.
    Instruct each engine to make n draws from the standard normal
    distribution, then hand back the mean, minimum, and maximum draws
    to the client. Return the results in three lists.
    
    Parameters:
        n (int): number of draws to make
        
    Returns:
        means (list of float): the mean draws of each engine
        mins (list of float): the minimum draws of each engine
        maxs (list of float): the maximum draws of each engine.
    """
    def draw(k):
        sample = np.random.normal(size=k)
        return sample.mean(), sample.min(), sample.max()


    dview = initialize()
    dview.block = True
    dview.execute("import numpy as np")
    stuff = dview.map(draw, [n for jj in range(len(dview))])
    means = []
    mins = []
    maxs = []
    for i in stuff:
        means.append(i[0])
        mins.append(i[1])
        maxs.append(i[2])
    
    return means, mins, maxs
    
# Problem 4
def prob4():
    """
    Time the process from the previous problem in parallel and serially for
    n = 1000000, 5000000, 10000000, and 15000000. To time in parallel, use
    your function from problem 3 . To time the process serially, run the drawing
    function in a for loop N times, where N is the number of engines on your machine.
    Plot the execution times against n.
    """
    vals = [1000000, 5000000,10000000,15000000]
    times_parallel = []
    times_serial = []
    dview = initialize()
    N = len(dview)
    for n in vals: # go through each n

        # parallel
        start = time.time()
        prob3(n)
        times_parallel.append(time.time()-start)
    
        # serial
        start = time.time()
        means = []
        mins = []
        maxs = []
        samples = []
        for jkl in range(N):
            sample = np.random.normal(size=n)
            samples.append((sample.mean(), sample.min(), sample.max()))
            for i in samples:
                means.append(i[0])
                mins.append(i[1])
                maxs.append(i[2])
        times_serial.append(time.time() - start)

    plt.plot(vals, times_parallel, label="parallel")
    plt.plot(vals, times_serial, label="serial")
    plt.legend()
    plt.xlabel('# of computations n')
    plt.ylabel('Time')
    plt.title("Times to Compute")
    plt.show()


# Problem 5
def parallel_trapezoidal_rule(f, a, b, n=200):
    """
    Write a function that accepts a function handle, f, bounds of integration,
    a and b, and a number of points to use, n. Split the interval of
    integration among all available processors and use the trapezoidal
    rule to numerically evaluate the integral over the interval [a,b].

    Parameters:
        f (function handle): the function to evaluate
        a (float): the lower bound of integration
        b (float): the upper bound of integration
        n (int): the number of points to use; defaults to 200
    Returns:
        value (float): the approximate integral calculated by the
            trapezoidal rule
    """
    x = np.linspace(a,b,n)

    # parallelize
    dview = initialize()
    dview.block = True
    dview.execute("import numpy as np")
    stuff = dview.map(f, x)
    h = (b-a)/(n-1)
    answer = h*(np.sum(stuff)-0.5*(f(a) + f(b))) 
    return answer
