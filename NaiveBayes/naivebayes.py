# Spencer Halverson
# 11/16/2021

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from scipy import stats
from sklearn.base import ClassifierMixin
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB

class NaiveBayesFilter(ClassifierMixin):
    '''
    A Naive Bayes Classifier that sorts messages in to spam or ham.
    '''

    def __init__(self):
        return 

    def fit(self, X, y):
        '''
        Create a table that will allow the filter to evaluate P(H), P(S)
        and P(w|C)

        Parameters:
            X (pd.Series): training data
            y (pd.Series): training labels
        '''
        # get N_vocab
        self.vocab_set = set()
        for row in X:
            words = row.split(" ")
            for word in words:
                # if word == '':
                #     continue
                self.vocab_set.add(word)
        vocab_list = list(self.vocab_set) 
        N_vocab = len(vocab_list)
        vocab_dict = {vocab_list[i]:i for i in range(N_vocab)} 
        # -- Usage --
        # vocab_list[num] = word
        # vocab_dict[word] = num

        self.spam_perc = len(y=='spam')/len(y)
        self.ham_perc = len(y=='ham')/len(y)
        self.spam_word_total = 0 
        self.ham_word_total = 0 # count every word (not necessarily unique) that appears in ham
        

        # create DataFrame filter
        indices= ['spam', 'ham']
        cols = vocab_list
        self.data = pd.DataFrame(0, index=indices, columns=cols)
        # print(self.data)

        # fill DataFrame
        for index in X.index:
            row = X[index]
            label = y[index]
            words = row.split(" ")
            if label == 'ham':
                self.ham_word_total += len(words)
            elif label == 'spam':
                self.spam_word_total += len(words)
            else:
                raise ValueError("label is {}".format(label))
            for word in words:
                # if word == '':
                #     continue
                self.data.loc[label, word] += 1
            
        return self
        
    def predict_proba(self, X):
        '''
        Find P(C=k|x) for each x in X and for each class k by computing
        P(C=k)P(x|C=k)

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,2): Probability each message is ham, spam
                0 column is ham
                1 column is spam
        '''
        # cond_prob_spam_x = self.spam_perc*cond_prob_x_spam

        predictions = np.zeros((X.shape[0], 2))
        for i, x in enumerate(X):
            cond_prob_spam_x = self.spam_perc
            cond_prob_ham_x = self.ham_perc
            # compute P(S|x), P(H|x)
            words = x.split(" ")
            unique_words, unique_word_count = np.unique(words, return_counts=True)
            for j in range(len(unique_words)):
                word = unique_words[j]
                if word in self.vocab_set:
                    cond_prob_x_spam = self.data[word]['spam'] / self.spam_word_total
                    cond_prob_x_ham = self.data[word]['ham'] / self.ham_word_total
                else:
                    # set conditional probability for each to 1
                    cond_prob_x_spam = 1.0
                    cond_prob_x_ham = 1.0
                
                # find relative conditional probabilities
                cond_prob_spam_x *= cond_prob_x_spam**unique_word_count[j]
                cond_prob_ham_x *= cond_prob_x_ham**unique_word_count[j]
        
            # add relative percents to matrix
            predictions[i, 0] = cond_prob_ham_x
            predictions[i, 1] = cond_prob_spam_x

        return predictions

    def predict(self, X):
        '''
        Use self.predict_proba to assign labels to X,
        the label will be a string that is either 'spam' or 'ham'

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,): label for each message
        '''
        # get probabilities for each, then take argmax
        percs = self.predict_proba(X)
        nums = np.argmax(percs, axis=1)
        predictions = []

        # assign the words 'ham' and 'spam'
        for i in nums:
            if i == 1:
                predictions.append('spam')
            else:
                predictions.append('ham')
        return np.array(predictions)
            
        
    def predict_log_proba(self, X):
        '''
        Find ln(P(C=k|x)) for each x in X and for each class k

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,2): Probability each message is ham, spam
                0 column is ham
                1 column is spam
        '''
        # cond_prob_spam_x = self.spam_perc*cond_prob_x_spam

        predictions = np.zeros((X.shape[0], 2))
        for i, x in enumerate(X):
            cond_prob_spam_x = np.log(self.spam_perc)
            cond_prob_ham_x = np.log(self.ham_perc)
            # compute P(S|x), P(H|x)
            words = x.split(" ")
            unique_words, unique_word_count = np.unique(words, return_counts=True)
            for j in range(len(unique_words)):
                word = unique_words[j]
                if word in self.vocab_set:
                    cond_prob_x_spam = self.data[word]['spam'] / self.spam_word_total
                    cond_prob_x_ham = self.data[word]['ham'] / self.ham_word_total
                else:
                    # set conditional probability for each to 1
                    cond_prob_x_spam = 1.0
                    cond_prob_x_ham = 1.0
                
                # find relative conditional log probabilities
                cond_prob_spam_x += unique_word_count[j]*np.log(cond_prob_x_spam)
                cond_prob_ham_x += unique_word_count[j]*np.log(cond_prob_x_ham)
        
            # add relative percents to matrix
            predictions[i, 0] = cond_prob_ham_x
            predictions[i, 1] = cond_prob_spam_x

        return predictions
        

    def predict_log(self, X):
        '''
        Use self.predict_log_proba to assign labels to X,
        the label will be a string that is either 'spam' or 'ham'

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,): label for each message
        '''
        # get log probabilities for each, then take argmax
        percs = self.predict_log_proba(X)
        nums = np.argmax(percs, axis=1)
        predictions = []

        # assign the words 'ham' and 'spam'
        for i in nums:
            if i == 1:
                predictions.append('spam')
            else:
                predictions.append('ham')
        return np.array(predictions)


class PoissonBayesFilter(ClassifierMixin):
    '''
    A Naive Bayes Classifier that sorts messages in to spam or ham.
    This classifier assumes that words are distributed like 
    Poisson random variables
    '''

    def __init__(self):
        return

    
    def fit(self, X, y):
        '''
        Uses bayesian inference to find the poisson rate for each word
        found in the training set. For this we will use the formulation
        of l = rt since we have variable message lengths.

        This method creates a tool that will allow the filter to 
        evaluate P(H), P(S), and P(w|C)


        Parameters:
            X (pd.Series): training data
            y (pd.Series): training labels
        
        Returns:
            self: this is an optional method to train
        '''
        # get N_vocab
        self.vocab_set = set()
        for row in X:
            words = row.split(" ")
            for word in words:
                # if word == '':
                #     continue
                self.vocab_set.add(word)
        vocab_list = list(self.vocab_set) 
        N_vocab = len(vocab_list)
        vocab_dict = {vocab_list[i]:i for i in range(N_vocab)} 
        # -- Usage --
        # vocab_list[num] = word
        # vocab_dict[word] = num

        self.spam_perc = len(y=='spam')/len(y)
        self.ham_perc = len(y=='ham')/len(y)
        self.spam_word_total = 0 
        self.ham_word_total = 0 # count every word (not necessarily unique) that appears in ham

        # create DataFrame filter
        indices= ['spam', 'ham']
        cols = vocab_list
        data = pd.DataFrame(0, index=indices, columns=cols)
        # print(data)

        # fill DataFrame
        for index in X.index:
            row = X[index]
            label = y[index]
            words = row.split(" ")
            if label == 'ham':
                self.ham_word_total += len(words)
            elif label == 'spam':
                self.spam_word_total += len(words)
            else:
                raise ValueError("label is {}".format(label))
            for word in words:
                # if word == '':
                #     continue
                data.loc[label, word] += 1

        # now that we have data, let's compute Poisson rates
        self.rates = data.copy()
        r = np.linspace(0.0001, 1, 1000) # numerically try different values of r

        for i in range(N_vocab):
            word = vocab_list[i]
            # calculate for ham and spam
            n_i_ham = data.loc['ham', word]
            n_i_spam = data.loc['spam', word]

            ## estimate best r_i_ham and spam in [0, 1]

            # r_i_ham = np.argmax(((r*self.ham_word_total)**n_i_ham) * np.exp(-1*r*self.ham_word_total) / np.math.factorial(n_i_ham))
            # r_i_spam = np.argmax(((r*self.spam_word_total)**n_i_spam) * np.exp(-1*r*self.spam_word_total) / np.math.factorial(n_i_spam))
            r_i_ham = np.argmax( n_i_ham*(np.log(r)+np.log(self.ham_word_total)) + -1*r*self.ham_word_total - np.sum([np.log(kk) for kk in range(1,n_i_ham + 1)]) )
            r_i_spam = np.argmax( n_i_spam*(np.log(r)+np.log(self.spam_word_total)) + -1*r*self.spam_word_total - np.sum([np.log(kk) for kk in range(1,n_i_spam + 1)]) )
            # assign values
            self.rates.loc['ham', word] = r[r_i_ham]
            self.rates.loc['spam', word] = r[r_i_spam]
            self.ham_rates = self.rates.loc['ham']
            self.spam_rates = self.rates.loc['spam']
        
        return self
    
    def predict_proba(self, X):
        '''
        Find P(C=k|x) for each x in X and for each class

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,2): Probability each message is ham or spam
                column 0 is ham, column 1 is spam 
        '''
        # cond_prob_spam_x = self.spam_perc*cond_prob_x_spam

        predictions = np.zeros((X.shape[0], 2))
        for i, x in enumerate(X):
            cond_prob_spam_x = np.log(self.spam_perc)
            cond_prob_ham_x = np.log(self.ham_perc)
            # compute P(S|x), P(H|x)
            words = x.split(" ")
            N = len(words)
            unique_words, unique_word_count = np.unique(words, return_counts=True)
            for j in range(len(unique_words)):
                word = unique_words[j]
                if word in self.vocab_set:
                    r_i_ham = self.ham_rates[word]
                    r_i_spam = self.spam_rates[word]
                    n_i = unique_word_count[i]

                    cond_prob_x_ham = n_i*(np.log(r_i_ham)+np.log(N)) + -1*r_i_ham*N - np.sum([np.log(kk) for kk in range(1,n_i + 1)])
                    cond_prob_x_spam = n_i*(np.log(r_i_spam)+np.log(N)) + -1*r_i_spam*N - np.sum([np.log(kk) for kk in range(1,n_i + 1)])
            
                else:
                    # set conditional probability for each to 1
                    cond_prob_x_spam = 0
                    cond_prob_x_ham = 0
                
                # find relative conditional log probabilities
                cond_prob_spam_x += cond_prob_x_spam
                cond_prob_ham_x += cond_prob_x_ham
        
            # add relative percents to matrix
            predictions[i, 0] = cond_prob_ham_x
            predictions[i, 1] = cond_prob_spam_x

        return predictions

    def predict(self, X):
        '''
        Use self.predict_proba to assign labels to X

        Parameters:
            X (pd.Series)(N,): messages to classify
        
        Return:
            (ndarray)(N,): label for each message
        '''
        # get log probabilities for each, then take argmax
        percs = self.predict_proba(X)
        nums = np.argmax(percs, axis=1)
        predictions = []

        # assign the words 'ham' and 'spam'
        for i in nums:
            if i == 1:
                predictions.append('spam')
            else:
                predictions.append('ham')
        return np.array(predictions)



def sklearn_method(X_train, y_train, X_test):
    '''
    Use sklearn's methods to transform X_train and X_test, create a
    naïve Bayes filter, and classify the provided test set.

    Parameters:
        X_train (pandas.Series): messages to train on
        y_train (pandas.Series): labels for X_train
        X_test  (pandas.Series): messages to classify

    Returns:
        (ndarray): classification of X_test
    '''
    # transform data
    vectorizer = CountVectorizer()
    transform_X_train = vectorizer.fit_transform(X_train)

    # input data to model
    model = MultinomialNB()
    model.fit(transform_X_train, y_train)

    # predict
    transform_X_test = vectorizer.transform(X_test)
    predictions = model.predict(transform_X_test)

    return predictions
